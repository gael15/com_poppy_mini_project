#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 11:25:07 2022

@authors: Guetin and Zingbeu
"""

# =============================================================================
# CoM of Robot Poppy using DH_modified parameters
# =============================================================================

# =============================================================================
# Librairies
# =============================================================================
from math import *
import pypot.vrep.io as vrep_io
import numpy as np
from time import *
import vrep_poppy
import pypot
import matplotlib.pyplot as plt
# =============================================================================
# Abdomen + left arm
# =============================================================================

def CoM_abdomen_l_arm(theta1, theta2, theta3, theta4, theta5, theta6, theta7,theta8,theta9):#TODO
    
    M =  3500#Total mass of robot poppy
    m1 = 1.677e-1
    m2 = 3.841e-2
    m3 = 9.268e-3
    m4 = 1.589e-1
    m5 = 2.629e-1
    m6 =8.436e-3
    m7 =8.281e-2
    m8 =1.681e-1
    m9 =4.865e-2
   
    C1 = np.array([[2.151e-3], [3.69e-3], [2.49e-3], [1]])
    C2 = np.array([[1.288e-3], [-2.111e-2], [-1.045e-3], [1]])
    C3 = np.array([[2.719e-3], [-3.637e-5], [1.77e-2], [1]])
    C4 = np.array([[-1.303e-3], [-2.778e-3], [-2.623e-3], [1]])
    C5 = np.array([[7.304e-3], [1.46e-2], [-3.433e-4], [1]])
    C6 = np.array([[5.003e-3], [9.725e-3], [-6.532e-4], [1]])
    C7 = np.array([[-2.072e-4], [1.663e-3], [2.395e-3], [1]])
    C8 = np.array([[-3.894e-4], [8.904e-4], [7.828e-5], [1]])
    C9 = np.array([[-6.438e-3], [4.516e-3], [8.832e-3], [1]])
    
    theta=np.zeros(5)
    
    theta[0] = theta1
    theta[1] = theta2
    theta[2] = theta3
    theta[3] = theta4
    theta[4] = theta5
    
    theta = np.array([theta[0], theta[1], theta[2], theta[3], theta[4]])
    
    alpha = np.array([0, pi/2, -pi/2, pi/2, pi/2])
    
    a = np.array([0, 0.05164+0.017+0.0028, 0, 0, 0])
    
    r = np.array([0, 0, 0, 0.07985+0.061, 0])
    
    a0 = 0.05
    a1 = 0.004
    a3 = 0.0185 - 0.01
    r1 = 0.0771
    r3 = 0.0284 + 0.03625 + 0.11175
    
    T01 = np.array([[cos(theta[0]), -sin(theta[0]), 0, a[0]], [cos(alpha[0])*sin(theta[0]), cos(alpha[0])*cos(theta[0]), -sin(alpha[0]) , -r[0]*sin(alpha[0])], [sin(alpha[0])*sin(theta[0]), sin(alpha[0])*cos(theta[0]), cos(alpha[0]), r[0]*cos(alpha[0])], [0, 0, 0, 1]])
    
    T12 = np.array([[cos(theta[1]), -sin(theta[1]), 0, a[1]], [cos(alpha[1])*sin(theta[1]), cos(alpha[1])*cos(theta[1]), -sin(alpha[1]) , -r[1]*sin(alpha[1])], [sin(alpha[1])*sin(theta[1]), sin(alpha[1])*cos(theta[1]), cos(alpha[1]), r[1]*cos(alpha[1])], [0, 0, 0, 1]])
        
    T23 = np.array([[cos(theta[2]), -sin(theta[2]), 0, a[2]], [cos(alpha[2])*sin(theta[2]), cos(alpha[2])*cos(theta[2]), -sin(alpha[2]) , -r[2]*sin(alpha[2])], [sin(alpha[2])*sin(theta[2]), sin(alpha[2])*cos(theta[2]), cos(alpha[2]), r[2]*cos(alpha[2])], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta[3]), -sin(theta[3]), 0, a[3]], [cos(alpha[3])*sin(theta[3]), cos(alpha[3])*cos(theta[3]), -sin(alpha[3]) , -r[3]*sin(alpha[3])], [sin(alpha[3])*sin(theta[3]), sin(alpha[3])*cos(theta[3]), cos(alpha[3]), r[3]*cos(alpha[3])], [0, 0, 0, 1]])
        
    T45 = np.array([[cos(theta[4]), -sin(theta[4]), 0, a[4]], [cos(alpha[4])*sin(theta[4]), cos(alpha[4])*cos(theta[4]), -sin(alpha[4]) , -r[4]*sin(alpha[4])], [sin(alpha[4])*sin(theta[4]), sin(alpha[4])*cos(theta[4]), cos(alpha[4]), r[4]*cos(alpha[4])], [0, 0, 0, 1]])
   
    T56 = np.array([[-sin(theta6), -cos(theta6), 0, a0],[cos(theta6), -sin(theta6), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T67 = np.array([[-cos(theta7), sin(theta7), 0, a1],[0, 0, 1, 0],[sin(theta7), cos(theta2), 0, 0],[0, 0, 0, 1]])
    
    T78 = np.array([[sin(theta8), cos(theta8), 0, 0], [0, 0, 1, r3], [cos(theta8), -sin(theta8), 0, 0],[0, 0, 0, 1]])
    
    T89 = np.array([[sin(theta9), cos(theta9), 0, a3], [0, 0, 1, 0], [cos(theta9), -sin(theta9), 0, 0],[0, 0, 0, 1]])
    
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45) 
    
    T06 = np.dot(T05,T56)
    
    T07 = np.dot(T06,T67)
    
    T08 = np.dot(T07,T78)
    
    T09 = np.dot(T08,T89)
 
    #Computing of CoM
    CoM1 = ((m1/M)*T01@C1) + ((m2/M)*T01@T02@C2) + ((m3/M)*T01@T02@T03@C3) + ((m4/M)*T01@T02@T03@T04@C4) + ((m5/M)*T01@T03@T03@T04@T05@C5) + ((m6/M)*T01@T03@T03@T04@T05@T06@C6) + ((m7/M)*T01@T03@T03@T04@T05@T06@T07@C7) + ((m8/M)*T01@T03@T03@T04@T05@T06@T07@T08@C8) + ((m9/M)*T01@T03@T03@T04@T05@T06@T07@T08@T09@C9)
    
    return CoM1

# =============================================================================
# Abdomen + right arm
# =============================================================================

def CoM_abdomen_r_arm(theta1, theta2, theta3, theta4, theta5, theta10, theta11,theta12,theta13):#TODO
    
    M =  3500#Total mass of robot poppy
    m1 = 1.677e-1
    m2 = 3.841e-2
    m3 = 9.268e-3
    m4 = 1.589e-1
    m5 = 2.629e-1
    
    m6 =8.482e-3
    m7 =8.281e-2
    m8 =1.681e-1
    m9 =4.865e-2
   
    C1 = np.array([[2.151e-3], [3.69e-3], [2.49e-3], [1]])
    C2 = np.array([[1.288e-3], [-2.111e-2], [-1.045e-3], [1]])
    C3 = np.array([[2.719e-3], [-3.637e-5], [1.77e-2], [1]])
    C4 = np.array([[-1.303e-3], [-2.778e-3], [-2.623e-3], [1]])
    C5 = np.array([[7.304e-3], [1.46e-2], [-3.433e-4], [1]])
    C6 = np.array([[4.359e-4], [-9.723e-3], [-6.666e-4], [1]])
    C7 = np.array([[-2.056e-4], [1.494e-3], [2.391e-3], [1]])
    C8 = np.array([[-3.731e-4], [-9.526e-4], [8.127e-5], [1]])
    C9 = np.array([[-7.128e-3], [-6.009e-3], [8.552e-3], [1]])
    
    theta=np.zeros(5)
    
    theta[0] = theta1
    theta[1] = theta2
    theta[2] = theta3
    theta[3] = theta4
    theta[4] = theta5
    
    theta = np.array([theta[0], theta[1], theta[2], theta[3], theta[4]])
    
    alpha = np.array([0, pi/2, -pi/2, pi/2, pi/2])
    
    a = np.array([0, 0.05164+0.017+0.0028, 0, 0, 0])
    
    r = np.array([0, 0, 0, 0.07985+0.061, 0])
    
    a0 = 0.05
    a1 = 0.004
    a3 = 0.0185 - 0.01
    r1 = 0.0771
    r3 = 0.0284 + 0.03625 + 0.11175
    
    T01 = np.array([[cos(theta[0]), -sin(theta[0]), 0, a[0]], [cos(alpha[0])*sin(theta[0]), cos(alpha[0])*cos(theta[0]), -sin(alpha[0]) , -r[0]*sin(alpha[0])], [sin(alpha[0])*sin(theta[0]), sin(alpha[0])*cos(theta[0]), cos(alpha[0]), r[0]*cos(alpha[0])], [0, 0, 0, 1]])
    
    T12 = np.array([[cos(theta[1]), -sin(theta[1]), 0, a[1]], [cos(alpha[1])*sin(theta[1]), cos(alpha[1])*cos(theta[1]), -sin(alpha[1]) , -r[1]*sin(alpha[1])], [sin(alpha[1])*sin(theta[1]), sin(alpha[1])*cos(theta[1]), cos(alpha[1]), r[1]*cos(alpha[1])], [0, 0, 0, 1]])
        
    T23 = np.array([[cos(theta[2]), -sin(theta[2]), 0, a[2]], [cos(alpha[2])*sin(theta[2]), cos(alpha[2])*cos(theta[2]), -sin(alpha[2]) , -r[2]*sin(alpha[2])], [sin(alpha[2])*sin(theta[2]), sin(alpha[2])*cos(theta[2]), cos(alpha[2]), r[2]*cos(alpha[2])], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta[3]), -sin(theta[3]), 0, a[3]], [cos(alpha[3])*sin(theta[3]), cos(alpha[3])*cos(theta[3]), -sin(alpha[3]) , -r[3]*sin(alpha[3])], [sin(alpha[3])*sin(theta[3]), sin(alpha[3])*cos(theta[3]), cos(alpha[3]), r[3]*cos(alpha[3])], [0, 0, 0, 1]])
        
    T45 = np.array([[cos(theta[4]), -sin(theta[4]), 0, a[4]], [cos(alpha[4])*sin(theta[4]), cos(alpha[4])*cos(theta[4]), -sin(alpha[4]) , -r[4]*sin(alpha[4])], [sin(alpha[4])*sin(theta[4]), sin(alpha[4])*cos(theta[4]), cos(alpha[4]), r[4]*cos(alpha[4])], [0, 0, 0, 1]])
   
    T56 = np.array([[-sin(theta10), -cos(theta10), 0, a0],[cos(theta10), -sin(theta10), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T67 = np.array([[-cos(theta11), sin(theta11), 0, a1],[0, 0, -1, 0],[-sin(theta11), -cos(theta11), 0, 0],[0, 0, 0, 1]])
    
    T78 = np.array([[-sin(theta12), cos(theta12), 0, 0], [0, 0, -1, -r3], [-cos(theta12), -sin(theta12), 0, 0],[0, 0, 0, 1]])
   
    T89 = np.array([[-sin(theta13), -cos(theta13), 0, a3], [0, 0, -1, 0], [cos(theta13), -sin(theta13), 0, 0],[0, 0, 0, 1]])
 
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45) 
    
    T06 = np.dot(T05,T56)
    
    T07 = np.dot(T06,T67)
    
    T08 = np.dot(T07,T78)
    
    T09 = np.dot(T08,T89)
    
    #Computing of CoM
    CoM2 = ((m1/M)*T01@C1) + ((m2/M)*T01@T02@C2) + ((m3/M)*T01@T02@T03@C3) + ((m4/M)*T01@T02@T03@T04@C4) + ((m5/M)*T01@T03@T03@T04@T05@C5) + ((m6/M)*T01@T03@T03@T04@T05@T06@C6) + ((m7/M)*T01@T03@T03@T04@T05@T06@T07@C7) + ((m8/M)*T01@T03@T03@T04@T05@T06@T07@T08@C8) + ((m9/M)*T01@T03@T03@T04@T05@T06@T07@T08@T09@C9)
    
    return CoM2


# =============================================================================
# Left leg of poppy
# =============================================================================

def CoM_l_leg(theta14, theta15, theta16, theta17, theta18):
    
    M =  3500#Total mass of robot poppy
    m1 = 8.438e-2
    m2 = 8.317e-2
    m3 = 1.149e-1
    m4 = 1.156e-1
    m5 = 4.683e-2
    
    C1 = np.array([[1.68e-3], [1.927e-4], [1.679e-2], [1]])
    C2 = np.array([[8.337e-4], [-7.705e-4], [3.538e-3], [1]])
    C3 = np.array([[-3.211e-3], [5.799e-3], [5.322e-2], [1]])
    C4 = np.array([[1.292e-3], [-7.914e-3], [-4.674e-2], [1]])
    C5 = np.array([[-1.070e-2], [7.516e-4], [-2.343e-3], [1]])
    
    theta = np.array([0, pi/2, -pi/2 , 0, 0])
    
    alpha = np.array([0, -pi/2, pi/2, 0, 0])
    
    a0 =0.0225417
    a1 =0.04399986
    a3 = -0.182
    a4 =-0.18
    r1 =-0.005
    r2 =0.024
    
    
    T01 = np.array([[cos(theta14), -sin(theta14), 0, a0],[sin(theta14), cos(theta14), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T12 = np.array([[-sin(theta15), -cos(theta15), 0, a1], [0, 0, 1 , r2], [-cos(theta15), sin(theta15), 0, 0], [0, 0, 0, 1]])
        
    T23 = np.array([[sin(theta16), -cos(theta16), 0, 0], [0, 0, 1 , 0], [cos(theta16), sin(theta16), 0, 0], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta17), -sin(theta17), 0, a3],[sin(theta17), cos(theta17), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
        
    T45 =np.array([[cos(theta18), -sin(theta18), 0, a4],[sin(theta18), cos(theta18), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
   
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45)
    
    #Computing of CoM
    #CoM1 = ((m1/M)*np.dot(T01,C1)) + ((m2/M)*np.dot(np.dot(T01,T02),C2)) + ((m3/M)*np.dot(np.dot(np.dot(T01,T02),T03),C3)) + ((m4/M)*np.dot(np.dot(np.dot(np.dot(T01,T02),T03),T04),C4)) + ((m5/M)*np.dot(T05,C5))
    CoM3 = ((m1/M)*T01@C1) + ((m2/M)*T01@T02@C2) + ((m3/M)*T01@T02@T03@C3) + ((m4/M)*T01@T02@T03@T04@C4) + ((m5/M)*T01@T03@T03@T04@T05@C5)
    
    return CoM3

# =============================================================================
# Rigth leg of Poppy
# =============================================================================

def CoM_r_leg(theta19, theta20, theta21, theta22, theta23):
    
    M =  3500#Total mass of robot poppy
    m1 = 8.438e-2
    m2 = 8.317e-2
    m3 = 1.164e-1
    m4 = 1.156e-1
    m5 = 4.679e-2
   
    C1 = np.array([[1.678e-3], [1.051e-4], [-1.684e-2], [1]])
    C2 = np.array([[8.339e-4], [6.129e-4], [3.571e-3], [1]])
    C3 = np.array([[-3.396e-3], [5.975e-3], [-5.183e-2], [1]])
    C4 = np.array([[-1.353e-3], [-7.861e-3], [-4.677e-2], [1]])
    C5 = np.array([[-1.073e-2], [-6.26e-4], [-2.289e-3], [1]])
       
    a0 =0.0225417
    a1 =0.04399986
    a3 = 0.182
    a4 =0.18
    
    r1 =-0.005
    r2 =-0.024
    
    T01 = np.array([[cos(theta19), -sin(theta19), 0, a0],[sin(theta19), cos(theta19), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T12 = np.array([[sin(theta20), -cos(theta20), 0, a1], [0, 0, -1 , r2], [cos(theta20), sin(theta20), 0, 0], [0, 0, 0, 1]])
        
    T23 = np.array([[-sin(theta21), -cos(theta21), 0, 0], [0, 0, 1 , 0], [-cos(theta21), sin(theta21), 0, 0], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta22), -sin(theta22), 0, a3],[sin(theta22), cos(theta22), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
        
    T45 = np.array([[cos(theta23), -sin(theta23), 0, a4],[sin(theta23), cos(theta23), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45)
  
    #Computing of CoM
    CoM4 = ((m1/M)*T01@C1) + ((m2/M)*T01@T02@C2) + ((m3/M)*T01@T02@T03@C3) + ((m4/M)*T01@T02@T03@T04@C4) + ((m5/M)*T01@T03@T03@T04@T05@C5)
    
    return CoM4

# =============================================================================
# Abdomen + head
# =============================================================================
def CoM_abdomen_head (theta1, theta2, theta3, theta4, theta5, theta24, theta25): #TODO
    M =  3500#Total mass of robot poppy
    m1 = 1.677e-1
    m2 = 3.841e-2
    m3 = 9.268e-3
    m4 = 1.589e-1
    m5 = 2.629e-1
    m6 = 5.885e-3
    m7 = 1.886e-1
    
    C1 = np.array([[2.151e-3], [3.69e-3], [2.49e-3], [1]])
    C2 = np.array([[1.288e-3], [-2.111e-2], [-1.045e-3], [1]])
    C3 = np.array([[2.719e-3], [-3.637e-5], [1.77e-2], [1]])
    C4 = np.array([[-1.303e-3], [-2.778e-3], [-2.623e-3], [1]])
    C5 = np.array([[7.304e-3], [1.46e-2], [-3.433e-4], [1]])
    C6 = np.array([[2.894e-6], [-6.465e-3], [-1.474e-3], [1]])
    C7 = np.array([[2.664e-3], [-4.232e-3], [-8.902e-4], [1]])
    
    theta=np.zeros(5)
    
    theta[0] = theta1
    theta[1] = theta2
    theta[2] = theta3
    theta[3] = theta4
    theta[4] = theta5
    
    theta = np.array([theta[0], theta[1], theta[2], theta[3], theta[4]])
    
    alpha = np.array([0, pi/2, -pi/2, pi/2, pi/2])
    
    a = np.array([0, 0.05164+0.017+0.0028, 0, 0, 0])
    
    r = np.array([0, 0, 0, 0.07985+0.061, 0])
    
    T01 = np.array([[cos(theta[0]), -sin(theta[0]), 0, a[0]], [cos(alpha[0])*sin(theta[0]), cos(alpha[0])*cos(theta[0]), -sin(alpha[0]) , -r[0]*sin(alpha[0])], [sin(alpha[0])*sin(theta[0]), sin(alpha[0])*cos(theta[0]), cos(alpha[0]), r[0]*cos(alpha[0])], [0, 0, 0, 1]])
    
    T12 = np.array([[cos(theta[1]), -sin(theta[1]), 0, a[1]], [cos(alpha[1])*sin(theta[1]), cos(alpha[1])*cos(theta[1]), -sin(alpha[1]) , -r[1]*sin(alpha[1])], [sin(alpha[1])*sin(theta[1]), sin(alpha[1])*cos(theta[1]), cos(alpha[1]), r[1]*cos(alpha[1])], [0, 0, 0, 1]])
        
    T23 = np.array([[cos(theta[2]), -sin(theta[2]), 0, a[2]], [cos(alpha[2])*sin(theta[2]), cos(alpha[2])*cos(theta[2]), -sin(alpha[2]) , -r[2]*sin(alpha[2])], [sin(alpha[2])*sin(theta[2]), sin(alpha[2])*cos(theta[2]), cos(alpha[2]), r[2]*cos(alpha[2])], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta[3]), -sin(theta[3]), 0, a[3]], [cos(alpha[3])*sin(theta[3]), cos(alpha[3])*cos(theta[3]), -sin(alpha[3]) , -r[3]*sin(alpha[3])], [sin(alpha[3])*sin(theta[3]), sin(alpha[3])*cos(theta[3]), cos(alpha[3]), r[3]*cos(alpha[3])], [0, 0, 0, 1]])
        
    T45 = np.array([[cos(theta[4]), -sin(theta[4]), 0, a[4]], [cos(alpha[4])*sin(theta[4]), cos(alpha[4])*cos(theta[4]), -sin(alpha[4]) , -r[4]*sin(alpha[4])], [sin(alpha[4])*sin(theta[4]), sin(alpha[4])*cos(theta[4]), cos(alpha[4]), r[4]*cos(alpha[4])], [0, 0, 0, 1]])
  
    ##Computing
    
    r1 = 0.084 + 0.0199999999999999
    
    T56 = np.array([[cos(theta24), -sin(theta24), 0, 0],[sin(theta24), cos(theta24), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T67 = np.array([[cos(theta25), -sin(theta25), 0, 0],[0, 0, -1, 0], [sin(theta25), cos(theta25), 0, 0],[0, 0, 0, 1]])
    
    
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45) 
    
    T06 = np.dot(T05,T56)
    
    T07 = np.dot(T06,T67)
    
    #Computing of CoM
    CoM5 = ((m1/M)*T01@C1) + ((m2/M)*T01@T02@C2) + ((m3/M)*T01@T02@T03@C3) + ((m4/M)*T01@T02@T03@T04@C4) + ((m5/M)*T01@T03@T03@T04@T05@C5) + ((m6/M)*T01@T03@T03@T04@T05@T06@C6) + ((m7/M)*T01@T03@T03@T04@T05@T06@T07@C7)
    
    return CoM5

if __name__ == '__main__' :
    
    #Etablissement de la connection avec Vrep ou CoppeliaSim
    poppy = vrep_io.VrepIO('127.0.0.1', 19997,'poppy_humanoid.ttt')
    
    #Démarrage de la simulation
    poppy.start_simulation()
    
    j=1
    
    N=80000
    
    XX = np.zeros((3,N))
    
    T = np.zeros((1,N))

    #Définition du temps 
    tf = 30 #secondes
    
    #Définition du clock en syncronisation avec vrep
    t0 = vrep_poppy.Time(poppy).time()
   
    while vrep_poppy.Time(poppy).time()-t0<tf:
       
        t=vrep_poppy.Time(poppy).time()-t0
           
        T[0,j] = t
        
        abdomen_arms = ['abs_y','abs_x','abs_z','bust_y','bust_x','l_shoulder_y', 'l_shoulder_x', 'l_arm_z', 'l_elbow_y','r_shoulder_y', 'r_shoulder_x', 'r_arm_z', 'r_elbow_y']
        
        l_leg =['l_hip_x', 'l_hip_z', 'l_hip_y', 'l_knee_y', 'l_ankle_y']
        
        r_leg = ['r_hip_x', 'r_hip_z', 'r_hip_y', 'r_knee_y', 'r_ankle_y']
        
        head_poppy  =['head_z', 'head_y']
       
        theta1=poppy.get_motor_position(abdomen_arms[0])
            
        theta2=poppy.get_motor_position(abdomen_arms[1])
            
        theta3=poppy.get_motor_position(abdomen_arms[2])
            
        theta4=poppy.get_motor_position(abdomen_arms[3])
        
        theta5=poppy.get_motor_position(abdomen_arms[4])
            
        theta6=poppy.get_motor_position(abdomen_arms[5])
            
        theta7=poppy.get_motor_position(abdomen_arms[6])
            
        theta8=poppy.get_motor_position(abdomen_arms[7])
        
        theta9=poppy.get_motor_position(abdomen_arms[8])
        
        theta10=poppy.get_motor_position(abdomen_arms[9])
            
        theta11=poppy.get_motor_position(abdomen_arms[10])
            
        theta12=poppy.get_motor_position(abdomen_arms[11])
        
        theta13=poppy.get_motor_position(abdomen_arms[12])
        
        theta14=poppy.get_motor_position(l_leg[0])
        
        theta15=poppy.get_motor_position(l_leg[1])
        
        theta16=poppy.get_motor_position(l_leg[2])
        
        theta17=poppy.get_motor_position(l_leg[3])
        
        theta18=poppy.get_motor_position(l_leg[4])
        
        theta19=poppy.get_motor_position(r_leg[0])
        
        theta20=poppy.get_motor_position(r_leg[1])
        
        theta21=poppy.get_motor_position(r_leg[2])
        
        theta22=poppy.get_motor_position(r_leg[3])
        
        theta23=poppy.get_motor_position(r_leg[4])
        
        theta24 = poppy.get_motor_position(head_poppy[0])
        
        theta25 = poppy.get_motor_position(head_poppy[1])
        
        CoM1  = CoM_abdomen_l_arm(theta1, theta2, theta3, theta4, theta5, theta6, theta7, theta8, theta9)
        CoM2  = CoM_abdomen_r_arm(theta1, theta2, theta3, theta4, theta5, theta10, theta11,theta12,theta13)
        CoM3 = CoM_l_leg(theta14, theta15, theta16, theta17, theta18)
        CoM4 = CoM_r_leg(theta19, theta20, theta21, theta22, theta23)
        CoM5  = CoM_abdomen_head(theta1, theta2, theta3, theta4, theta5, theta24, theta25)
        
        print('CoM of abdomen & left arm is =',CoM1)
        print('CoM of abdomen & right arm is =',CoM2)
        print('CoM of left leg is =',CoM3)
        print('CoM of right leg is =',CoM4)
        print('CoM of head is =',CoM5)
    
# =============================================================================
# Computing of total center of mass of robot Poppy
# =============================================================================

        CoM = CoM1 + CoM2 + CoM3 + CoM4 + CoM5
        print("Center of Mass Robot Poppy is =",CoM)
        
        XX[0,j]=CoM[0,0]
       
        XX[1,j]=CoM[1,0]
       
        XX[2,j]=CoM[2,0]
        
        j=j+1
# =============================================================================
# Polting of axes of center of mass
# =============================================================================

    plt.figure(1)
    plt.suptitle('Visualization of CoM using DH-parameters')
    plt.subplot(311)
    plt.plot(T[0,5:j-1],XX[0,5:j-1],'r',label ='CoM around x',linewidth=1.5)
    plt.title('Visualization of CoM on x')
    plt.xlabel('Time (s)')
    plt.ylabel('Movement(m)')
    plt.legend(loc='upper left')
    
    plt.subplot(312)
    plt.plot(T[0,5:j-1],XX[1,5:j-1],'g',label ='CoM around y',linewidth=1.5)
    plt.title('Visualization of CoM on y')
    plt.xlabel('Time (s)')
    plt.ylabel('Movement(m)')
    plt.legend(loc='upper left')
    
    plt.subplot(313)
    plt.plot(T[0,5:j-1],XX[2,5:j-1],'b',label ='CoM around z',linewidth=1.5)
    plt.title('Visualization of CoM on z')
    plt.xlabel('Time (s)')
    plt.ylabel('Movement(m)')
    plt.legend(loc='upper left')